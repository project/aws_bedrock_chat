<?php

namespace Drupal\aws_bedrock_chat;

use Aws\BedrockAgentRuntime\BedrockAgentRuntimeClient;
use Aws\Credentials\Credentials;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides responses for the AWS Bedrock Chat module.
 */
class BedrockClient {

  /**
   * The Bedrock Agent Runtime client.
   *
   * @var \Aws\BedrockAgentRuntime\BedrockAgentRuntimeClient
   */
  private $bedrockClient;

  /**
   * Stores Drupal configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * The Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new BedrockClient object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('aws_bedrock_chat.settings');
  }

  /**
   * Get the response message.
   *
   * @param string $prompt
   *   The user prompt text to be processed.
   * @param string $existing_session_id
   *   The existing session ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getResponse(string $prompt, string $existing_session_id = '') {

    // Process the user input and return response message.
    if (!empty($prompt)) {
      // Get configuration values.
      $disable_api = $this->config->get('disable_api') ?: FALSE;
      $debug = $this->config->get('debug') ?: FALSE;
      $bedrock_type = $this->config->get('bedrock_type') ?: '';
      $region = $this->config->get('region') ?: '';
      $authentication_method = $this->config->get('aws_authentication_method') ?: '';

      if ($bedrock_type == 'agent') {
        $agent_alias_id = $this->config->get('agent_alias_id') ?: '';
        $agent_id = $this->config->get('agent_id') ?: '';
        $session_id_prefix = $this->config->get('session_id_prefix') ?: '';
      }
      elseif ($bedrock_type == 'knowledge_base') {
        $knowledge_base_id = $this->config->get('knowledge_base_id') ?: '';
        $model_arn = $this->config->get('model_arn') ?: '';
        $search_type = $this->config->get('search_type') ?: '';
      }

      // Build configuration for BedrockAgentRuntimeClient.
      $client_config = [
        'region' => $region,
        'version' => 'latest',
      ];

      if ($authentication_method === 'profile') {
        $profile = $this->config->get('profile') ?: '';
        if (!empty($profile)) {
          $client_config['profile'] = $profile;
        }
      }
      elseif ($authentication_method === 'keys' || $authentication_method === 'env') {
        $api_key = $authentication_method === 'keys' ? $this->config->get('aws_access_key') : getenv('AWS_ACCESS_KEY_ID');
        $api_secret = $authentication_method === 'keys' ? $this->config->get('aws_secret_key') : getenv('AWS_SECRET_ACCESS_KEY');
        $api_session_token = $authentication_method === 'keys'
          ? ($this->config->get('aws_session_token') ?: NULL)
          : (getenv('AWS_SESSION_TOKEN') ?: NULL);
        if (!empty($api_key) && !empty($api_secret)) {
          $credentials = new Credentials($api_key, $api_secret, $api_session_token);
          $client_config['credentials'] = $credentials;
        }
      }

      if ($debug) {
        error_log('User message: ' . $prompt);
        error_log('Region: ' . $region);
        error_log('Authentication Method: ' . $authentication_method);
        error_log('Client config: ' . print_r($client_config, 1));
        if ($bedrock_type == 'agent') {
          error_log('Agent Alias ID: ' . $agent_alias_id);
          error_log('Agent ID: ' . $agent_id);
          error_log('Session ID Prefix: ' . $session_id_prefix);
        }
        elseif ($bedrock_type == 'knowledge_base') {
          error_log('Knowledge Base ID: ' . $knowledge_base_id);
          error_log('Model ARN: ' . $model_arn);
          error_log('Search Type: ' . $search_type);
        }
      }

      if (!$disable_api && !empty($region)) {
        if (class_exists('Aws\BedrockAgentRuntime\BedrockAgentRuntimeClient')) {
          try {
            // Initialize BedrockAgentRuntimeClient.
            $this->bedrockClient = new BedrockAgentRuntimeClient($client_config);

            if ($bedrock_type == 'knowledge_base' && !empty($model_arn) && !empty($knowledge_base_id) && !empty($search_type)) {
              // Retrieve and generate response.
              $result = $this->bedrockClient->retrieveAndGenerate([
                'input' => [
                  'text' => $prompt,
                ],
                'retrieveAndGenerateConfiguration' => [
                  'knowledgeBaseConfiguration' => [
                    'knowledgeBaseId' => $knowledge_base_id,
                    'modelArn' => $model_arn,
                    'retrievalConfiguration' => [
                      'vectorSearchConfiguration' => [
                        'overrideSearchType' => $search_type,
                      ],
                    ],
                  ],
                  'type' => 'KNOWLEDGE_BASE',
                ],
              ]);
            }
            elseif ($bedrock_type == 'agent' && !empty($agent_alias_id) && !empty($agent_id)) {
              // Invoke the agent.
              if (!empty($existing_session_id)) {
                $session_id = $existing_session_id;
              }
              else {
                $session_id = uniqid($session_id_prefix);
              }
              $result = $this->bedrockClient->invokeAgent([
                'agentAliasId' => $agent_alias_id,
                'agentId' => $agent_id,
                'inputText' => $prompt,
                'sessionId' => $session_id,
              ]);
            }
          }
          catch (\Throwable $e) {
            // Client error.
            if ($debug) {
              error_log('Error: ' . $e->getMessage());
            }
            return ['error' => 'client_error'];
          }
          if ($debug) {
            error_log('Client request completed successfully');
            if ($bedrock_type == 'agent') {
              error_log('Agent Result:');
              foreach ($result['completion'] as $event) {
                if (isset($event['accessDeniedException'])) {
                  error_log('AccessDeniedException Returned');
                }
                elseif (isset($event['badGatewayException'])) {
                  error_log('BadGatewayException Returned');
                }
                elseif (isset($event['chunk'])) {
                  error_log('Chunk Returned:');
                  error_log($event['chunk']['bytes']);
                }
                elseif (isset($event['conflictException'])) {
                  error_log('ConflictException Returned');
                }
                elseif (isset($event['dependencyFailedException'])) {
                  error_log('DependencyFailedException Returned');
                }
                elseif (isset($event['files'])) {
                  error_log('Files Returned');
                }
                elseif (isset($event['internalServerException'])) {
                  error_log('InternalServerException Returned');
                }
                elseif (isset($event['resourceNotFoundException'])) {
                  error_log('ResourceNotFoundException Returned');
                }
                elseif (isset($event['returnControl'])) {
                  error_log('ReturnControl Returned');
                }
                elseif (isset($event['serviceQuotaExceededException'])) {
                  error_log('ServiceQuotaExceededException Returned');
                }
                elseif (isset($event['throttlingException'])) {
                  error_log('ThrottlingException Returned');
                }
                elseif (isset($event['trace'])) {
                  error_log('Trace Returned');
                }
                elseif (isset($event['validationException'])) {
                  error_log('ValidationException Returned');
                }
              }
              if (isset($result['sessionId'])) {
                error_log('sessionId: ' . $result['sessionId']);
              }
            }
            elseif ($bedrock_type == 'knowledge_base') {
              error_log('Knowledge Base Result: ' . print_r($result, 1));
            }
          }
        }
        else {
          // BedrockAgentRuntimeClient class does not exist.
          if ($debug) {
            error_log('BedrockAgentRuntimeClient class does not exist, returning default message.');
          }
          return ['error' => 'sdk_missing'];
        }
      }
      else {
        // Chat API is disabled or parameter requirements not met.
        if ($debug) {
          error_log('Chat API is disabled or parameter requirements not met, returning default message.');
        }
        return ['error' => 'api_disabled'];
      }
    }
    else {
      // No user input.
      return ['error' => 'no_user_input'];
    }
    return $result;
  }

}
